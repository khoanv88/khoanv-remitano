
## Introduction

This is web which show list video, you can register account and share your video.

Default account: abc@gmail.com / 123456.

## Prerequisites

Frontend use React(v18.2.0)
Backend use Nodejs, express(v4.18.2) and ws(v8.13.0)

## Installation & Configuration

Frontend:
    - npm install
    - npm start

Backend:
    - npm install
    - node server.js

after that access link: http://localhost:3000

## Database Setup
Data stored in json file. There are 2 files: users.json and videos.json

## Docker Deployment
I have Dockerfile to config run app in Docker

## Usage
Main function of app
    - Dashboard: show list video
    - Login: login to app
    - Register: register account use email/password
    - Share: share your video. When user share video, all other users will be received notification message.

## Note
I deploy backend to free host, so it will take time go call Rest API. Please wait a second when you access bellow web: https://khoanv-remitano.vercel.app/dashbroad

