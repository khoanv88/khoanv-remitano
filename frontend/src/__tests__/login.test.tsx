import React from 'react';
import { render, screen, waitFor } from "@testing-library/react";
import Login from "../pages/login";
import userEvent from "@testing-library/user-event";
import '@testing-library/jest-dom';

test("Email input should be rendered", () => {
    render(<Login />);
    const emailInputEl = screen.getByPlaceholderText(/Email/i);
    expect(emailInputEl).toBeInTheDocument();
});

test("Password input should be rendered", () => {
    render(<Login />);
    const passwordInputEl = screen.getByPlaceholderText(/Password/i);
    expect(passwordInputEl).toBeInTheDocument();
});

test("Button should be rendered", () => {
    render(<Login />);
    const buttonEl = screen.getByRole("button");
    expect(buttonEl).toBeInTheDocument();
});

