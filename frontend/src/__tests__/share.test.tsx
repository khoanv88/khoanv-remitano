import React from 'react';
import { render, screen, waitFor } from "@testing-library/react";
import ShareVideo from "../pages/share";
import userEvent from "@testing-library/user-event";
import '@testing-library/jest-dom';

test("URL input should be rendered", () => {
    render(<ShareVideo />);
    const urlInputEl = screen.getByPlaceholderText(/Youtube video link/i);
    expect(urlInputEl).toBeInTheDocument();
});


test("Button should be rendered", () => {
    render(<ShareVideo />);
    const buttonEl = screen.getByRole("button");
    expect(buttonEl).toBeInTheDocument();
});