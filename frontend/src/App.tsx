import React  from 'react';
import { Route, Routes, BrowserRouter, Navigate } from 'react-router-dom';
import { RouteUrl } from './components/constants';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import {ToastContainer} from "react-toastify";

if (typeof window !== "undefined") {
	require("bootstrap/dist/js/bootstrap.bundle.min");
}

const loading = (
	<div className="pt-3 text-center">
		<div className="sk-spinner sk-spinner-pulse"></div>
	</div>
)

const Login = React.lazy(() => import('./pages/login'));
const Register = React.lazy(() => import('./pages/register'));
const ShareVideo = React.lazy(() => import('./pages/share'));
const Dashbroad = React.lazy(() => import('./pages/dashbroad'));

const RouterDom = () => (
	<React.Suspense fallback={loading}>
		<Routes>
			<Route path="/" element={<Navigate to={RouteUrl.DASHBROAD} />} />
			<Route path={RouteUrl.LOGIN} element={<Login />} />
			<Route path={RouteUrl.REGISTER} element={<Register />} />
			<Route path={RouteUrl.SHARE} element={<ShareVideo />} />
      		<Route path={RouteUrl.DASHBROAD} element={<Dashbroad />} />
		</Routes>
	</React.Suspense>
)

const App = () => {

	return (
        <BrowserRouter>
			<RouterDom/>
            <ToastContainer theme="colored"/>
        </BrowserRouter>
    )
}

export default App;