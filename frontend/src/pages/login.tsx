import { memo, useState } from "react";
import Header from "../components/header";
import * as Notify from "../components/notify";
import { empty } from "../components/utils";
import * as ApiService from "../services/api";
import { useNavigate } from "react-router-dom";
import { RouteUrl } from "../components/constants";

const Login = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate();

    const validateLoginInfo = () => {
        if (empty(email) || empty(password)) {
            Notify.error("Email or Password is invalid");
            return false;
        }
        return true;
    }

    const doLogin = () => {
        if (validateLoginInfo()) {
            ApiService.login(email, password).then((res: any) => {
                console.log(res.data);
                if (res.data?.result === "success") {
                    localStorage.setItem("email", email);
                    localStorage.setItem("token", res.data?.token);
                    navigate(RouteUrl.DASHBROAD);
                } else Notify.error("Email or Password is invalid");
            }, error => {

            })
        }
    }

    return (
        <div className="px-5">
            <Header />
            <section className="vh-100">
                <div className="container py-5 h-100">
                    <div className="row d-flex justify-content-center align-items-center h-100">
                        <div className="col-12 col-md-8 col-lg-6 col-xl-5">
                            <div className="card shadow-2-strong" style={{ "borderRadius": "0.5rem" }}>
                                <div className="card-body p-5 text-center">
                                    <h3 className="mb-4">Login to Funny Videos</h3>
                                    <div className="form-outline mb-4">
                                        <input type="email" placeholder="Email" className="form-control form-control-lg"
                                            value={email} onChange={(e) => setEmail(e.target.value)}
                                        />
                                    </div>

                                    <div className="form-outline mb-4">
                                        <input type="password" placeholder="Password" className="form-control form-control-lg"
                                            value={password} onChange={(e) => setPassword(e.target.value)}
                                        />
                                    </div>

                                    <button className="btn btn-primary btn-lg btn-block" type="submit" onClick={doLogin} style={{ "width": "150px", "color": "white" }}>Login</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default memo(Login);