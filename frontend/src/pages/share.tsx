import { memo, useEffect, useState } from "react";
import Header from "../components/header";
import { useNavigate } from "react-router-dom";
import { empty } from "../components/utils";
import { RouteUrl } from "../components/constants";
import * as Notify from "../components/notify";
import * as ApiService from "../services/api";
import { VideoInfo } from "../components/interface";

const ShareVideo = () => {
    const [url, setUrl] = useState("");
    const [email, setEmail] = useState("");
    const navigate = useNavigate();

    useEffect(() => {
        let cacheEmail = localStorage.getItem("email");
        if (cacheEmail) {
            setEmail(cacheEmail);
        } else navigate(RouteUrl.LOGIN);
    }, [])


    const getYouTubeId = (url: string) => {
        let tmp = url.match(/(?:https?:\/\/)?(?:www\.|m\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\/?\?v=|\/embed\/|\/)([^\s&\\?\\/\\#]+)/);
        if (tmp) return tmp[1];
        else return "";
    }

    const doShare = () => {
        if (validateUrl()) {
            let videoId = getYouTubeId(url);
            ApiService.getYoutubeVideo(videoId).then(res => {
                if (res.data?.items !== null && res.data?.items.length > 0) {
                    let item = res.data?.items[0];
                    let video = {
                        title: item.snippet?.title,
                        description: item.snippet?.description,
                        email: email,
                        videoId: videoId,
                        url: url,
                        numLikes: item.statistics?.likeCount,
                        numDislikes: item.statistics?.dislikeCount
                    };
                    shareVideo(video);
                } else {
                    Notify.error("Youtube URL is invalid");
                    return false;
                }
            })
        }
    }

    const shareVideo = (video: VideoInfo) => {
        console.log(video);
        ApiService.shareVideo(video).then(res => {
            if (res.data?.result === "success")
                navigate(RouteUrl.DASHBROAD);
            else Notify.error("Share video fail");
        })
    }

    const validateUrl = () => {
        if (empty(url)) {
            Notify.error("Youtube URL is empty");
            return false;
        }
        if (!checkYoutubeUrl(url)) {
            Notify.error("Youtube URL is invalid");
            return false;
        }
        return true;
    }

    const checkYoutubeUrl = (url: string) => {
        var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        if (url.match(p)) {
            return true;
        }
        return false;
    }

    return (
        <div className="px-5">
            <Header />

            <section className="vh-100">
                <div className="container py-5 h-100">
                    <div className="row d-flex justify-content-center align-items-center h-100">
                        <div className="col-12 col-md-8 col-lg-6 col-xl-6">
                            <div className="card shadow-2-strong" style={{ "borderRadius": "0.5rem" }}>
                                <div className="card-body p-5 text-center">
                                    <h3 className="mb-4">Share Youtube video</h3>
                                    <div className="form-outline mb-4">
                                        <input type="text" placeholder="Youtube video link" className="form-control form-control-lg"
                                            value={url} onChange={(e) => setUrl(e.target.value)}
                                        />
                                    </div>

                                    <button className="btn btn-primary btn-lg btn-block" type="submit" onClick={doShare} style={{ "width": "150px", "color": "white" }}>Share</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default memo(ShareVideo);