import { memo, useEffect, useState } from "react";
import Header from "../components/header";
import { VideoInfo } from "../components/interface";
import * as ApiService from "../services/api";

const Dashbroad = () => {
    const [videos, setVideos] = useState<VideoInfo[]>([]);

    useEffect(() => {
        getVideos();
    }, [])

    const getVideos = () => {
        ApiService.getVideos().then(res => {
            setVideos(res.data);
        }, error => { })
    }

    return (
        <div className="px-5">
            <Header />
            <br />
            <div className="w-75 mx-auto">
                {videos.map((video, idx) => {
                    return (
                        <div key={idx} className="bg-light mb-3">
                            <div className="d-flex">
                                <div className="col-6 p-3">
                                    <div >
                                        <iframe src={'https://www.youtube-nocookie.com/embed/' + video.videoId + "?enablejsapi=1"} frameBorder="0"
                                            title="Youtube Video" aria-label={'YouTube Video: ' + video.title}
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                            allowFullScreen style={{ "width": "100%", "height": "250px" }}>

                                        </iframe>
                                    </div>
                                </div>
                                <div className="col-6 p-3">
                                    <strong className="text-danger">{video.title.substring(0, 50) + "..."}</strong><br />
                                    <small className="font-italic d-flex mt-2">Shared by: {video.email}
                                    </small>
                                    <small className="d-flex">
                                        <span>{video.numLikes}</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="mr-4 mt-1"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg>
                                        <span className="ms-2">{video.numDislikes}</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="mr-4 mt-1"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg>
                                    </small>
                                    <span>Description</span><br />
                                    <span>{video.description.substring(0, 300)}</span>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default memo(Dashbroad);