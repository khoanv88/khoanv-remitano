import { toast } from 'react-toastify';

const timeout = 3000;

export const success = (msg:string) => {
    toast.success(msg, {
        position: "top-right",
        autoClose: timeout,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    });
}

export const error = (msg:string) => {
    toast.error(msg, {
        position: "top-right",
        autoClose: timeout,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    });
}