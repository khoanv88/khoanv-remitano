import { memo,useEffect,useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { RouteUrl } from "./constants";

const Header = () => {
    const [isLogin, setIsLogin] = useState(true);
    const [email, setEmail] = useState("");
    const navigate = useNavigate();

    useEffect(() => {
        let token = localStorage.getItem("token");
        let cacheEmail = localStorage.getItem("email");
        if(null === token || undefined === token){
            setIsLogin(false);
        } else {
            setIsLogin(true);
            setEmail(cacheEmail as string);
        } 
    }, [])

    const logout = () =>{
        localStorage.clear();
        navigate(RouteUrl.LOGIN);
    }

    const shareVideo = () => {
        navigate(RouteUrl.SHARE);
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <a className="navbar-brand" href="/"><strong>Funny Videos</strong></a>
            </div>
            {!isLogin && <div className="d-flex">
                <Link className="navbar-brand" to="/login"><span>Login</span></Link>
                <Link className="navbar-brand" to="/register">Register</Link>
            </div>}

            {isLogin && <div className="d-flex">
                <span className="mt-2">Wellcome:&nbsp;{email}</span>
                <button className="btn btn-outline-success ms-2" type="submit" onClick={shareVideo}>Share</button>
                <button className="btn btn-outline-danger ms-2 mx-3" type="submit" onClick={logout}>Logout</button>
            </div>}
        </nav>
    )
}
export default memo(Header);