export const empty = (value: string) => {
    return (undefined === value) || (null === value) || (0 === value.trim().length);
}

export const isNull = (value: any) => {
    return (undefined === value) || (null === value);
}

export const isEmail = (val: string) =>{
    let regEmail = /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!regEmail.test(val)){
      return false;
    }
    return true;
}