export interface VideoInfo {
    title: string;
    description: string;
    email: string;
    videoId: string;
    url: string;
    numLikes: string;
    numDislikes: string;
}