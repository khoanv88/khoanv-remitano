export const LocalStorageKey = {
    TOKEN: 'token',
    LOGIN_ID: "loginID"
}

export const HttpCode = {
    OK: 200,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401
}

export const RouteUrl = {
    LOGIN: "/login",
    REGISTER: "/register",
    DASHBROAD: "/dashbroad",
    SHARE: "/share"
}