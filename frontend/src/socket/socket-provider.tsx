import { useEffect, useState, createContext, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { LocalStorageKey, RouteUrl } from "../components/constants";
import { empty } from "../components/utils";
import * as Notify from "../components/notify";

const webSocket = new WebSocket(process.env.REACT_APP_WS_URL as string);
export const SocketContext = createContext(webSocket);

interface ISocketProvider {
    children: React.ReactNode;
}

const SOCKET_RECONNECTION_TIMEOUT = 3000;
const NUMBER_RECONNECT = 10;
let timer: any = null;
let socketTimer: any = null;

export const SocketProvider = (props: ISocketProvider) => {
    const [ws, setWs] = useState<WebSocket>(webSocket);

    const numberReconnect = useRef<number>(0);

    useEffect(() => {
        const onOpen = () => {
            console.log("Websocket opened");
            clearInterval(socketTimer);
            timer = setInterval(() => {
                ping();
            }, 30000);
        }

        const onClose = () => {
            console.log("Websocket closed");
            clearInterval(timer);

            let token = localStorage.getItem(LocalStorageKey.TOKEN)

            socketTimer = setInterval(() => {
                if (token) {
                    numberReconnect.current = numberReconnect.current + 1;

                    if (numberReconnect.current <= NUMBER_RECONNECT) {
                        setWs(new WebSocket(process.env.REACT_APP_WS_URL as string));
                    } else {
                        localStorage.clear();
                        window.location.href = RouteUrl.LOGIN;
                    }
                } else clearInterval(socketTimer)
            }, SOCKET_RECONNECTION_TIMEOUT);
        }

        if (ws) {
            ws.onopen = onOpen;
            ws.onclose = onClose;
            ws.onmessage = handleReceiveMessage;
        }
        return () => {
            if (ws) ws.close();
        };
    }, [ws]);

    const ping = () => {
        console.log("Sent ping")
        ws?.send("ping");
    }

    const handleReceiveMessage = (msg: any) => {
        Notify.success(msg.data)
    }

    return (
        <SocketContext.Provider value={ws}>{props.children}</SocketContext.Provider>
    );
}