import axios from 'axios';
export const API_URL = process.env.REACT_APP_API_URL;
const YOUTUBE_API_KEY = "AIzaSyCHNinUEGt6rbi-zorGXZ7nrNap7sdfX5g";

export const getVideos = () => {
    return axios({
        method: "GET",
        url: API_URL + '/videos',
        params: {
        }
    });
}

export const login = (email: string,password: string) => {
    return axios({
        method: "POST",
        url: API_URL + '/login',
        data: {
            email: email,
            password: password
        }
    });
}

export const register = (email: string,password: string) => {
    return axios({
        method: "POST",
        url: API_URL + '/register',
        data: {
            email: email,
            password: password
        }
    });
}

export const shareVideo = (video: any) => {
    return axios({
        method: "POST",
        url: API_URL + '/share-video',
        data: video
    });
}

export const getYoutubeVideo = (videoId: string) => {
    return axios({
        method: "GET",
        url: 'https://www.googleapis.com/youtube/v3/videos',
        params: {
            fields:"items(id,snippet(title,description),statistics(likeCount,dislikeCount))",
            part:"snippet,statistics",
            key:YOUTUBE_API_KEY,
            id:videoId
        }
    });
}