var express = require('express');
var cors = require('cors');
var uuid = require("uuid");
const WebSocket = require('ws');

var app = express();
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

var fs = require("fs");

const wsServer = new WebSocket.Server({ noServer: true });
wsServer.on('connection', socket => {
  socket.on('message', message => console.log(message));
});

empty = (value) =>{
	return (undefined === value) || (null === value) || (0 === value.trim().length);
}

app.get('/videos', function (req, res) {
   fs.readFile( __dirname + "/" + "videos.json", 'utf8', function (err, data) {
      res.end(data);
   });
})

app.post('/share-video', (req,res) =>{
	var postData = req.body;
	fs.readFile( __dirname + "/" + "videos.json", { mode: 0o666 }, (err, data) => {
		var listVideo = JSON.parse(data);
		listVideo.unshift(postData);
		var json = JSON.stringify(listVideo);
		fs.writeFile( __dirname + "/" + "videos.json", json, 'utf8', (error) =>{
			try {
				if(error){
					var resJson = {
						result: "fail",
						msg: "Share video fail"
					}
					res.end(JSON.stringify(resJson));
				}else{
					var resJson = {
						result: "success",
						msg: "Share video success"
					}
					res.end(JSON.stringify(resJson));

					wsServer.clients.forEach(function each(client) {
						if (client.readyState === WebSocket.OPEN) {
							let msg = postData.title + " - shared by " + postData.email;
						  	client.send(msg);
						}
					});
				}
			} catch (e){
				console.log(e)
			}
			
		});	
	})
})

app.post('/login', (req,res) =>{
	var postData = req.body;
	if(!empty(postData.email) && !empty(postData.password)){
		fs.readFile( __dirname + "/" + "users.json", 'utf8', (err, data) => {
			var listUser = JSON.parse(data);
			var userExist = false;

			listUser.forEach((user) => {
				if(user.email === postData.email && user.password === postData.password){
					userExist = true;
					return;
				}
			});

			if(userExist){
				var token = uuid.v4();
				var resJson = {
					result: "success",
					msg: "Login success",
					token: token
				}
				res.end(JSON.stringify(resJson));
			}else{
				var resJson = {
					result: "fail",
					msg: "Email or Password invalid"
				}
				res.end(JSON.stringify(resJson));
			}
		});
	}else{
		var resJson = {
			result: "fail",
			msg: "Email or Password invalid"
		}
		res.end(JSON.stringify(resJson));
	}
})

app.post('/register', (req, res) => {
	var postData = req.body;
	
	if(!empty(postData.email) && !empty(postData.password)){
		fs.readFile( __dirname + "/" + "users.json", 'utf8', (err, data) => {
			var listUser = JSON.parse(data);
			var userExist = false;

			listUser.forEach((user) => {
				if(user.email === postData.email){
					userExist = true;
					return;
				}
			});

			if(!userExist){
				listUser.push(postData);
				var json = JSON.stringify(listUser);
				fs.writeFile( __dirname + "/" + "users.json", json, { mode: 0o666 }, (error) =>{
					if(error){
						var resJson = {
							result: "fail",
							msg: "Register fail"
						}
						res.end(JSON.stringify(resJson));
					}else{
						var resJson = {
							result: "success",
							msg: "Register success"
						}
						res.end(JSON.stringify(resJson));
					}
				});
			}else{
				var resJson = {
					result: "fail",
					msg: "Email is existed"
				}
				res.end(JSON.stringify(resJson));
			}
		 });
	}else{
		var resJson = {
			result: "fail",
			msg: "Email or Password invalid"
		}
		res.end(JSON.stringify(resJson));
	}
 })

var server = app.listen(8081, () => {
   var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)
})

server.on('upgrade', (request, socket, head) => {
	wsServer.handleUpgrade(request, socket, head, socket => {
	  wsServer.emit('connection', socket, request);
	});
  });
